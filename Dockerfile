FROM i386/ubuntu:17.04

ADD scripts/setup_apt.sh scripts/install_useful_tools.sh scripts/install_build_deps.sh /
RUN /setup_apt.sh
RUN /install_useful_tools.sh
RUN /install_build_deps.sh

ENV QT_VERSION 5.6
ENV QTC_VERSION 4.5

WORKDIR /
ADD scripts/clone_qt.sh /
RUN QT_VERSION=$QT_VERSION /clone_qt.sh
ADD scripts/build_qt.sh /
RUN QT_VERSION=$QT_VERSION /build_qt.sh

ADD scripts/clone_qtc.sh /
RUN QTC_VERSION=$QTC_VERSION /clone_qtc.sh
ADD scripts/build_qtc.sh /
RUN QTC_VERSION=$QTC_VERSION /build_qtc.sh
ADD scripts/configure_qtc.sh /
RUN QTC_VERSION=$QTC_VERSION /configure_qtc.sh

ADD scripts/configure_system.sh /
RUN /configure_system.sh

ADD scripts/cleanup.sh /
RUN /cleanup.sh

# Add scripts/launch.sh /
# ENTRYPOINT /launch.sh
ENTRYPOINT /bin/bash