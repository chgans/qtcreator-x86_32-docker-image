QtCreator Docker Image
======================

This is a docker image to run QtCreator (4.5), Qbs (1.10), Qt (5.6) inside a x86_32/Ubuntu-17.04 OS

Comes with cmake and other build related tools.

Inspired by
===========
https://github.com/erstrom/docker-qt/

https://github.com/zachdeibert/docker-images

https://github.com/dannywillems/docker-qtcreator

ASCII Screenshot:
=================

```
Qt 5.6.4 (i386-little_endian-ilp32 shared (dynamic) release build; by GCC 6.3.0 20170406) on "xcb" 
OS: Ubuntu 17.04 [linux version 4.4.92-31-default]

Architecture: x86_64; features: SSE2 SSE3 SSSE3 SSE4.1 SSE4.2 AVX

Environment:
  QT_VERSION="5.6"
  QTC_VERSION="4.5"

Features: QT_NO_EXCEPTIONS QT_NO_ZLIB

Library info:
  PrefixPath: /opt/qt/5.6
  DocumentationPath: /opt/qt/5.6/doc
  HeadersPath: /opt/qt/5.6/include
  LibrariesPath: /opt/qt/5.6/lib
  LibraryExecutablesPath: /opt/qt/5.6/libexec
  BinariesPath: /opt/qt/5.6/bin
  PluginsPath: /opt/qt/5.6/plugins
  ImportsPath: /opt/qt/5.6/imports
  Qml2ImportsPath: /opt/qt/5.6/qml
  ArchDataPath: /opt/qt/5.6
  DataPath: /opt/qt/5.6
  TranslationsPath: /opt/qt/5.6/translations
  ExamplesPath: /opt/qt/5.6/examples
  TestsPath: /opt/qt/5.6/tests
  SettingsPath: /opt/qt/5.6/etc/xdg

Standard paths [*...* denote writable entry]:
  DesktopLocation: "Desktop" */root/Desktop*
  DocumentsLocation: "Documents" */root/Documents*
  FontsLocation: "Fonts" */root/.fonts*
  ApplicationsLocation: "Applications" */root/.local/share/applications* /usr/local/share/applications /usr/share/applications
  MusicLocation: "Music" */root/Music*
  MoviesLocation: "Movies" */root/Videos*
  PicturesLocation: "Pictures" */root/Pictures*
  TempLocation: "Temporary Directory" */tmp*
  HomeLocation: "Home" */root*
  AppLocalDataLocation: "Application Data" */root/.local/share/QtProject/qtdiag* /usr/local/share/QtProject/qtdiag /usr/share/QtProject/qtdiag
  CacheLocation: "Cache" */root/.cache/QtProject/qtdiag*
  GenericDataLocation: "Shared Data" */root/.local/share* /usr/local/share /usr/share
  RuntimeLocation: "Runtime" */tmp/runtime-root*
  ConfigLocation: "Configuration" */root/.config* /etc/xdg
  DownloadLocation: "Download" */root/Downloads*
  GenericCacheLocation: "Shared Cache" */root/.cache*
  GenericConfigLocation: "Shared Configuration" */root/.config* /etc/xdg
  AppDataLocation: "Application Data" */root/.local/share/QtProject/qtdiag* /usr/local/share/QtProject/qtdiag /usr/share/QtProject/qtdiag
  AppConfigLocation: "Application Configuration" */root/.config/QtProject/qtdiag* /etc/xdg/QtProject/qtdiag

File selectors (increasing order of precedence):
  C unix linux ubuntu

Network:
  Using "OpenSSL 1.0.2g  1 Mar 2016", version: 0x1000207f

Platform capabilities: ThreadedPixmaps OpenGL WindowMasks MultipleWindows ForeignWindows NonFullScreenWindows NativeWidgets WindowManagement SyncState RasterGLSurface SwitchableWidgetComposition

Style hints:
  mouseDoubleClickInterval: 400
  mousePressAndHoldInterval: 800
  startDragDistance: 10
  startDragTime: 500
  startDragVelocity: 0
  keyboardInputInterval: 400
  keyboardAutoRepeatRate: 30
  cursorFlashTime: 1000
  showIsFullScreen: 0
  showIsMaximized: 0
  passwordMaskDelay: 0
  passwordMaskCharacter: U+25CF
  fontSmoothingGamma: 1
  useRtlExtensions: 0
  setFocusOnTouchRelease: 0
  tabFocusBehavior: Qt::TabFocusBehavior(TabFocusAllControls) 
  singleClickActivation: 0

Additional style hints (QPlatformIntegration):
  ReplayMousePressOutsidePopup: 0

Theme:
  Available    : generic
  Styles       : Fusion,Windows
  System font  : "Sans Serif" 9

Fonts:
  General font : "Sans Serif" 9
  Fixed font   : "monospace" 9
  Title font   : "DejaVu Sans" 12
  Smallest font: "DejaVu Sans" 12

Palette:
  WindowText: #ff000000
  Button: #ffefebe7
  Light: #ffffffff
  Midlight: #ffcbc7c4
  Dark: #ff9f9d9a
  Mid: #ffb8b5b2
  Text: #ff000000
  BrightText: #ffffffff
  ButtonText: #ff000000
  Base: #ffffffff
  Window: #ffefebe7
  Shadow: #ff767472
  Highlight: #ff308cc6
  HighlightedText: #ffffffff
  Link: #ff0000ff
  LinkVisited: #ffff00ff
  AlternateBase: #fff7f5f3
  NoRole: #ff000000
  ToolTipBase: #ffffffdc
  ToolTipText: #ff000000

Screens: 2, High DPI scaling: inactive
# 0 "DVI-I-1" Depth: 24 Primary: yes
  Geometry: 1920x1080+1920+0 Available: 1920x1044+1920+0
  Virtual geometry: 3840x1080+0+0 Available: 3840x1044+0+0
  2 virtual siblings
  Physical size: 521x293 mm  Refresh: 60 Hz Power state: 0
  Physical DPI: 93.6046,93.6246 Logical DPI: 96,96.2526 Subpixel_None
  DevicePixelRatio: 1 Pixel density: 1
  Primary orientation: 2 Orientation: 2 Native orientation: 0 OrientationUpdateMask: 0

# 1 "DVI-D-1" Depth: 24 Primary: no
  Geometry: 1920x1080+0+0 Available: 1920x1044+0+0
  Virtual geometry: 3840x1080+0+0 Available: 3840x1044+0+0
  2 virtual siblings
  Physical size: 521x293 mm  Refresh: 60 Hz Power state: 0
  Physical DPI: 93.6046,93.6246 Logical DPI: 96,96.2526 Subpixel_None
  DevicePixelRatio: 1 Pixel density: 1
  Primary orientation: 2 Orientation: 2 Native orientation: 0 OrientationUpdateMask: 0

LibGL Vendor: nouveau
Renderer: Gallium 0.4 on NV126
Version: 3.0 Mesa 17.0.7
Shading language: 1.30
Format: Version: 3.0 Profile: 0 Swap behavior: 0 Buffer size (RGB): 8,8,8

QStandardPaths: XDG_RUNTIME_DIR not set, defaulting to '/tmp/runtime-root'
QStandardPaths: XDG_RUNTIME_DIR not set, defaulting to '/tmp/runtime-root'

Plugin information:

+ Android                 4.5.0
+ AutoTest                4.5.0
  AutotoolsProjectManager 4.5.0
  BareMetal               4.5.0
+ Bazaar                  4.5.0
  Beautifier              4.5.0
+ BinEditor               4.5.0
+ Bookmarks               4.5.0
+ CMakeProjectManager     4.5.0
+ CVS                     4.5.0
+ ClangStaticAnalyzer     4.5.0
+ ClassView               4.5.0
  ClearCase               4.5.0
+ CodePaster              4.5.0
+ Core                    4.5.0
+ CppEditor               4.5.0
+ CppTools                4.5.0
+ Debugger                4.5.0
+ Designer                4.5.0
+ DiffEditor              4.5.0
  EmacsKeys               4.5.0
+ FakeVim                 4.5.0
+ GLSLEditor              4.5.0
+ GenericProjectManager   4.5.0
+ Git                     4.5.0
  HelloWorld              4.5.0
+ Help                    4.5.0
+ ImageViewer             4.5.0
  Ios                     4.5.0
+ Macros                  4.5.0
+ Mercurial               4.5.0
  ModelEditor             4.5.0
  Nim                     4.5.0
+ Perforce                4.5.0
+ ProjectExplorer         4.5.0
+ PythonEditor            4.5.0
+ QbsProjectManager       4.5.0
+ QmakeAndroidSupport     4.5.0
+ QmakeProjectManager     4.5.0
+ QmlDesigner             4.5.0
+ QmlJSEditor             4.5.0
+ QmlJSTools              4.5.0
+ QmlProfiler             4.5.0
+ QmlProjectManager       4.5.0
+ Qnx                     4.5.0
+ QtSupport               4.5.0
+ RemoteLinux             4.5.0
+ ResourceEditor          4.5.0
  ScxmlEditor             4.5.0
  SilverSearcher          4.5.0
+ Subversion              4.5.0
+ TaskList                4.5.0
+ TextEditor              4.5.0
  Todo                    4.5.0
  UpdateInfo              4.5.0
+ Valgrind                4.5.0
+ VcsBase                 4.5.0
+ Welcome                 4.5.0
  WinRt                   4.5.0

Qt Creator 4.5.0
Based on Qt 5.6.4 (GCC 6.3.0 20170406, 32 bit)

```

TODO
====

Dockerfile:
 - libxcb-xinerama0-dev already installed
 - proper dash/bash replacement

build from tarball:
 - http://download.qt.io/official_releases/qt/5.6/5.6.0/single/qt-everywhere-opensource-src-5.6.0.7z
 - http://download.qt.io/official_releases/qtcreator/4.5/4.5.0/qt-creator-opensource-src-4.5.0.tar.xz

Qtc:
 - build sdktool
 - setup qtc with toolchain/qt/kit
 - setup qbs profiles
 - use docker multi stage build