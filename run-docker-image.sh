#!/bin/sh

set -e
set -x

docker run -it --privileged --rm \
    -v /tmp/.X11-unix/:/tmp/.X11-unix \
    -e DISPLAY=unix$DISPLAY \
    navico/dev-builder \
    /bin/bash
