#!/bin/bash

set -e
set -x

SCRIPT_PATH=`readlink -f $0`
SCRIPT_DIR=`dirname $SCRIPT_PATH`

[[ -z ${QT_TOP_DIR+x} ]] && QT_TOP_DIR=$SCRIPT_DIR
[[ -z ${QT_VERSION+x} ]] && QT_VERSION="master",
[[ -z ${QT_J_LEVEL+x} ]] && QT_J_LEVEL=$((`nproc`+1))
[[ -z ${QT_WORKING_DIR+x} ]] && QT_WORKING_DIR="qt5"
[[ -z ${QT_BUILD_DIR+x} ]] && QT_BUILD_DIR="$QT_WORKING_DIR/build-${QT_VERSION}"
[[ -z ${QT_CONFIGURE_OPTS+x} ]] && QT_CONFIGURE_OPTS="-opensource -nomake examples -nomake tests -confirm-license"

mkdir -p $QT_BUILD_DIR
pushd $QT_BUILD_DIR

../configure $QT_CONFIGURE_OPTS -prefix "/opt/qt/${QT_VERSION}"
make -j $QT_J_LEVEL
make install
