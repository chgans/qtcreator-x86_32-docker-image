#!/bin/bash

set -e
set -x

[[ -z ${QTC_VERSION+x} ]] && QTC_VERSION="master"
[[ -z ${QTC_J_LEVEL+x} ]] && QTC_J_LEVEL=$((`nproc`+1))
[[ -z ${QTC_WORKING_DIR+x} ]] && QTC_WORKING_DIR="qtc-${QTC_VERSION}"

cd $QTC_WORKING_DIR
mkdir build
cd build
/opt/qt/5.6/bin/qmake -r ../qtcreator.pro QTC_PREFIX=/opt/qtcreator/
make -j $QTC_J_LEVEL
make install
