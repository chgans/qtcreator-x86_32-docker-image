#!/bin/bash

set -x
set -e

SCRIPT_PATH=`readlink -f $0`
SCRIPT_DIR=`dirname $SCRIPT_PATH`

# Configuration env vars will be set to default values if not defined.
[[ -z ${QT_TOP_DIR+x} ]] && QT_TOP_DIR=$SCRIPT_DIR
[[ -z ${QT_VERSION+x} ]] && QT_VERSION="master",
[[ -z ${QT_J_LEVEL+x} ]] && QT_J_LEVEL=$((`nproc`+1))
[[ -z ${QT_GIT_URL+x} ]] && QT_GIT_URL="https://code.qt.io/qt/qt5.git"
[[ -z ${QT_WORKING_DIR+x} ]] && QT_WORKING_DIR="qt5"
[[ -z ${QT_INIT_REPOSITORY_OPTS+x} ]] && QT_INIT_REPOSITORY_OPTS="--module-subset=default,-qt3d,-qtactiveqt,-qtandroidextras,-qtenginio,-qtfeedback,-qtmacextras,-qtpurchasing,-qtpim,-qtsensors,-qtserialbus,-qtwebchannel,-qtwebkit,-qtwebkit-examples,-qtwebsockets,-qtwebview,-qtwinextras"

pushd $QT_TOP_DIR
git clone $QT_GIT_URL $QT_WORKING_DIR
pushd $QT_WORKING_DIR
git checkout $QT_VERSION
perl init-repository $QT_INIT_REPOSITORY_OPTS
