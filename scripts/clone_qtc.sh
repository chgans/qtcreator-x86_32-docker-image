#!/bin/bash

set -e
set -x

[[ -z ${QTC_GIT_URL+x} ]] && QTC_GIT_URL="https://code.qt.io/qt-creator/qt-creator.git"
[[ -z ${QTC_VERSION+x} ]] && QTC_VERSION="master"
[[ -z ${QTC_WORKING_DIR+x} ]] && QTC_WORKING_DIR="qtc-${QTC_VERSION}"

git clone --recursive $QTC_GIT_URL $QTC_WORKING_DIR
pushd $QTC_WORKING_DIR
git checkout $QTC_VERSION
git submodule update --init
