#!/bin/bash

set -e
set -x

SDKTOOL="/opt/qtcreator/libexec/qtcreator/sdktool"

if [ ! -f "${SDKTOOL}" ]
then
    echo "Could not find sdktool (${SDKTOOL})"
    exit 1
fi

############# QtCreator variables
#
# Some of the values to use with sdktool directly come from the QtCreator source
# code. There are defined in variables below for conveniance and ease of maintenance.

# This is the engine id for GDB as defined in the enum Debugger::DebuggerEngineType
DEBUGGER_ENGINE_GDB=1

# This is the type key for Qt for embedded linux, defined in remotelinux_constants.h
QT_TYPE_EMBEDDED_LINUX="RemoteLinux.EmbeddedLinuxQt"

# This is the type key for Generic linux devices, defined in remotelinux_constants.h
DEVICE_TYPE_GENERIC_LINUX="GenericLinuxOsType"

# Language to setup toolchains
LANG_C=1
LANG_CPP=2

# Qt types: Dektop Linux
QT_DESKTOP=Qt4ProjectManager.QtVersion.Desktop

#
#############


create_toolchain()
{
    local id=$1
    local lang=$2
    local name=$3
    local path=$4
    local abi=$5

    ${SDKTOOL} rmTC "--id" "${id}" > /dev/null 2>&1 || true
    ${SDKTOOL} addTC \
        "--id" "${id}" \
        "--language" "${lang}" \
        "--name" "${name}" \
        "--path" "${path}" \
        "--abi" "${abi}" \
        "--supportedAbis" "${abi}"
}

create_debugger()
{
    local id=$1
    local name=$2
    local path=$3
    local abi=$4
    local engine=$5

    ${SDKTOOL} rmDebugger "--id" "${id}" > /dev/null 2>&1 || true
    ${SDKTOOL} addDebugger \
        "--id" "${id}" \
        "--name" "${name}" \
        "--engine" "${engine}" \
        "--binary" "${path}" \
        "--abis" "${abi}"
}

create_qt()
{
    local id=$1
    local name=$2
    local qmake=$3
    local type=$4

    ${SDKTOOL} rmQt "--id" "${id}" > /dev/null 2>&1 || true
    ${SDKTOOL} addQt \
        "--id" "${id}" \
        "--name" "${name}" \
        "--type" "${type}" \
        "--qmake" "${qmake}"
}

create_device()
{
    local id=$1
    local name=$2
    local username=$3
    local password=$4
    local ssh_port=$5

    ${SDKTOOL} rmDev "--id" "${id}" > /dev/null 2>&1 || true

    # don't know what is the meaning of:
    #  type: 0
    #  authentication: 3 (password protected instead of key?)
    #  origin: 0
    ${SDKTOOL} addDev \
        "--id" "${id}" \
        "--name" "${name}" \
        "--type" "0" \
        "--authentication" "3" \
        "--freePorts" "10000-10100" \
        "--host" "" \
        "--origin" "0" \
        "--osType" "${DEVICE_TYPE_GENERIC_LINUX}" \
        "--password" "${password}" \
        "--sshPort" "${ssh_port}" \
        "--timeout" "0" \
        "--uname" "${username}"
}

create_kit()
{
    local id=$1
    local name=$2
    local ctoolchain_id=$3
    local cxxtoolchain_id=$4
    local debugger_id=$5
    local qt_id=$6

    ${SDKTOOL} rmKit "--id" "${id}" > /dev/null 2>&1 || true
    ${SDKTOOL} addKit \
        "--id" "${id}" \
        "--name" "${name}" \
	"--devicetype" "Desktop" \
        "--debuggerid" "${debugger_id}" \
        "--Ctoolchain" "${ctoolchain_id}" \
        "--Cxxtoolchain" "${cxxtoolchain_id}" \
        "--qt" "${qt_id}" \
        "--mkspec" ""
}

setup_gcc_toolchain()
{
    local platform=$1
    local toolchain_prefix=$2
    local abi=$3
    local qmake=$4

    local gcc=${toolchain_prefix}gcc
    local gpp=${toolchain_prefix}g++
    local gdb=${toolchain_prefix}gdb

    if [ ! -f "${gcc}" ]
    then
        echo "Toolchain ${platform}: could not find GCC/C (${gcc})"
        return 1
    fi
    if [ ! -f "${gpp}" ]
    then
        echo "Toolchain ${platform}: could not find GCC/C++ (${gpp})"
        return 1
    fi
    if [ ! -f "${gdb}" ]
    then
        echo "Toolchain ${platform}: could not find GDB (${gdb})"
        return 1
    fi
    if [ ! -f "${qmake}" ]
    then
        echo "Toolchain ${platform}: could not find QMake (${qmake})"
        return 1
    fi

    local id_prefix="navico.${platform}."

    local c_toolchain_id="ProjectExplorer.ToolChain.Gcc:${id_prefix}gcc"
    local cxx_toolchain_id="ProjectExplorer.ToolChain.Gcc:${id_prefix}g++"
    local debugger_id="${id_prefix}gdb"
    local qt_id="${id_prefix}qt"
    local kit_id="${id_prefix}kit"

    create_toolchain "${c_toolchain_id}" ${LANG_C} "Navico GCC (C, ${platform})" "${gcc}" "${abi}"
    create_toolchain "${cxx_toolchain_id}" ${LANG_CPP} "Navico GCC (C++, ${platform})" "${gpp}" "${abi}"
    create_debugger "${debugger_id}" "Navico GDB (${platform})" "${gdb}" "${abi}" "${DEBUGGER_ENGINE_GDB}"
    create_qt "${qt_id}" "Qt %{Qt:Version} (Navico ${platform})" "${qmake}" "${QT_DESKTOP}"
    create_kit "${kit_id}" "Navico ${platform}" "${c_toolchain_id}" "${cxx_toolchain_id}" "${debugger_id}" "${qt_id}"
}

#
#############

setup_gcc_toolchain "i386" "/usr/bin/" "x86-linux-generic-elf-32bit" "/opt/qt/5.6/bin/qmake"

export PATH=/opt/qtcreator/bin/:$PATH
qbs-setup-toolchains /usr/bin/g++ i386-pc-linux-gcc
qbs-setup-qt /opt/qt/5.6/bin/qmake i386-pc-linux-gcc-qt-5.6
