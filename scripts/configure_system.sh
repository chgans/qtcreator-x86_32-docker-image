#!/bin/bash

set -e
set -x

cat >> /etc/bash.bashrc <<EOF

echo ""
echo "welcome to qtcreator-x86_32-docker-image:"
echo " - Qt 5.6"
echo " - QtCreator 4.5"
echo " - Qbs 1.10"
echo ""
export PATH=/opt/qtcreator/bin:/opt/qt/5.6/bin:$PATH

EOF
