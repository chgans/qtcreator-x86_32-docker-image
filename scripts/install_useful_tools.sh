#!/bin/sh

apt-get -y install \
    tmux \
    wget \
    zip \
    git \
    vim \
    emacs-nox \
    ccache \
    cmake \
    p7zip-full \
    tree \
    htop \
    bash \
    curl \
    dos2unix
