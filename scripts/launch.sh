#!/bin/bash

set -e 
set -x

echo $# $*

display=$1 
shift
user=$1
shift
command="$*"

useradd --create-home --shell /bin/bash "$user"
DISPLAY="$display" su "$user" -c "$command"
