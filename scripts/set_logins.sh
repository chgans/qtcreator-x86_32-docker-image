#!/bin/sh

## Use bash instead of dash for login shell, and force sh to point to bash 
chsh -s /bin/bash root
ln -sf bash /bin/sh

# Simple root password in case we want to customize the container
echo "root:root" | chpasswd

useradd -G video -ms /bin/bash user
