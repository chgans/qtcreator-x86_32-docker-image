#!/bin/sh

# Install required packages, using NZ mirrors, except for updates and security
# Enable source repo for build-dep
sed -i 's#http://archive.ubuntu.com/ubuntu/ zesty #http://nz.archive.ubuntu.com/ubuntu/ zesty #g' /etc/apt/sources.list
sed -i 's,^# deb-src,deb-src,g' /etc/apt/sources.list
apt-get -y upgrade
apt-get -y update
